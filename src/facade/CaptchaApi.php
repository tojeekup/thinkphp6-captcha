<?php

namespace ric\captcha\facade;

use think\Facade;

/**
 * Class Captcha
 * @package ric\captcha\facade
 * @mixin \ric\captcha\Captcha
 */
class CaptchaApi extends Facade
{
    protected static function getFacadeClass()
    {
        return \ric\captcha\CaptchaApi::class;
    }
}
